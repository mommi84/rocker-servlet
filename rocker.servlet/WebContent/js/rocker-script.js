rocker_base = "http://rocker.aksw.org";

selected_key = 0;
selected_issue = -1;

// this will allow to use a JS-only frontend
function get_url_parameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
            return sParameterName[1];
    }
}

function params() {
	dataset = decodeURIComponent(get_url_parameter("dataset").replace(/\+/g,  " "));
	cname = decodeURIComponent(get_url_parameter("cname").replace(/\+/g,  " "));
	alpha = get_url_parameter("alpha");
}

function view_resource(j) {
	
	selected_issue = j;
	$("#graph-panel").show();
	
	// button appearance
	for(var i=0; i<issues.length; i++) {
		if(i == j) {
			$("#iss"+i).addClass("active");
			$("#iss"+i).addClass("btn-primary");
			$("#iss"+i).removeClass("btn-default");
		} else {
			$("#iss"+i).removeClass("active");
			$("#iss"+i).removeClass("btn-primary");
			$("#iss"+i).addClass("btn-default");
		}
	}
	$("#iss-id").html((selected_key+1)+"."+(selected_issue+1));
	
	load_graph();
}

function select_key(j) {
	
	selected_key = j;
	selected_issue = -1;
	$("#issue-graph").html("");
	$("#graph-panel").hide();
	
	// button appearance
	for(var i=0; i<keys.length; i++) {
		if(i == j) {
			$("#key"+i).addClass("active");
			$("#key"+i).addClass("btn-primary");
			$("#key"+i).removeClass("btn-default");
		} else {
			$("#key"+i).removeClass("active");
			$("#key"+i).removeClass("btn-primary");
			$("#key"+i).addClass("btn-default");
		}
	}
	$("#iss-id").html("");
	$("#key-id").html(""+(selected_key+1));
	
	// load resources with issues
	issues = keys[j]["http:\/\/rocker.aksw.org\/ontology#hasIssues"];
	if(issues.length == 0)
		$("#resource-list").html("<tr><td colspan='3'>No issues found for this key. Try another key or decrease the threshold.</tr>");
	else
		$("#resource-list").html("");
	for(var i=0; i<issues.length; i++) {
		var issue_string = "";
		var iss = issues[i]["http:\/\/www.w3.org\/2004\/02\/skos\/core#member"];
		for(var j=0; j<iss.length; j++) {
			var ixxue = iss[j].replace("http://dbpedia.org/resource/", "dbr:");
			issue_string += ixxue + "&nbsp;&nbsp;<a href='" + iss[j] + "' target='_blank'><span class='glyphicon glyphicon-new-window' aria-hidden='true'></span></a><br>";
		}
		$("#resource-list").append("<tr><td>"+(i+1)+"<td>"+issue_string+"<td><button id=\"iss"+i+"\" type=\"button\" class=\"btn btn-xs btn-default\" onclick=\"view_resource("+i+");\">View issue</button></tr>");
	}
}

function load_everything() {

	//alert(dataset+"\n"+cname+"\n"+alpha);
	
	$.loader.open();
	
	var rocker_request = $.ajax({
		method: "GET",
		url: rocker_base + "/findKey",
		data: {
			dataset: dataset,
			cname: cname,
			alpha: alpha,
		}
	});
	rocker_request.done(function( resp ) {
		// alert(resp.toSource());
		$.loader.close();
		display_keys(resp);
	});
	rocker_request.fail(function( msg ) {
		alert("Error: " + msg.toSource());
	});
	
}

function display_keys(jsonObj) {
	
	var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(jsonObj));

	$('<a role="button" class="btn btn-primary btn-lg btn-block" href="data:' + data + '" download="keys.json">Export Keys</a>').appendTo('#export-div');
	
	keys = jsonObj["http:\/\/rocker.aksw.org\/ontology#hasKey"];
	for(var i=0; i<keys.length; i++) {
		var prop_string = "";
		var props = keys[i]["http:\/\/www.w3.org\/2004\/02\/skos\/core#member"];
		var score = keys[i]["http:\/\/rocker.aksw.org\/ontology#score"];
		for(var j=0; j<props.length; j++)
			prop_string += props[j] + "&nbsp;&nbsp;<a href='"+props[j]+"' target='_blank'><span class='glyphicon glyphicon-new-window' aria-hidden='true'></span></a><br>";
		if(i == 0) // first is active
			active = "btn-primary active";
		else
			active = "btn-default";
		$("#key-table").append("<tr><td>"+(i+1)+"<td>"+prop_string+"<td>"+props.length+"<td>"+score.toFixed(3)+"<td><button id=\"key"+i+"\" type=\"button\" class=\"btn btn-xs "+active+"\" onclick=\"select_key("+i+");\">Select</button></tr>");
		
	}
	// first is selected
	select_key(0);
}

pref = {
	"http://www.w3.org/1999/02/22-rdf-syntax-ns#": "rdf:",
	"http://dbpedia.org/ontology/": "dbo:",
	"http://dbpedia.org/resource/": "dbr:",
	"http://www.okkam.org/oaie/": "oaie:",
	"http://www.okkam.org/ontology_person1.owl#": "oaie-person1:",
	"http://www.okkam.org/ontology_restaurant.owl#": "oaie-restaurant:",
};

function replace_prefixes(here) {
	$.each( pref, function( url, prefix ) {
		here = here.replace(url, prefix);
	});
	return here;
}

function prefix(t) {
	$.each( t, function( key, value ) {
		t[key] = replace_prefixes(t[key]);
	});
	return t;
}

function load_graph() {
	
	// get selected issue
	
	issres = issues[selected_issue]["http:\/\/www.w3.org\/2004\/02\/skos\/core#member"];
	var keyres = keys[selected_key]["http:\/\/www.w3.org\/2004\/02\/skos\/core#member"];
	var subjects = [], predicates = [];
	for(var i=0; i<issres.length; i++)
		subjects.push(issres[i]);
	for(var i=0; i<keyres.length; i++)
		predicates.push(keyres[i]);
	
	// alert(subjects.join() + '\n' + predicates.join());
	
	$.loader.open();
	
	// alert(dataset);
	
	var graph_request = $.ajax({
		method: "GET",
		url: rocker_base + "/objSet",
		data: {
			dataset: dataset,
			subjects: subjects.join("::"),
			predicates: predicates.join("::"),
		}
	});
	graph_request.done(function( resp ) {
		// alert(resp.toSource());
		$.loader.close();
		draw_graph(resp);
	});
	graph_request.fail(function( msg ) {
		alert("Error: " + msg);
	});
	
}

function draw_graph(jsonGr) {
	
	triples = jsonGr["triples"];
	// apply prefixes
	var prefres = [];
	for(var i=0; i<triples.length; i++)
		triples[i] = prefix(triples[i]);
	for(var i=0; i<issres.length; i++)
		prefres[i] = replace_prefixes(issres[i])
		
	// deduplicate
	var subj = [], obj = [];
	for(var i=0; i<triples.length; i++) {
		var t = triples[i];
		if($.inArray(t["s"], subj) == -1 && $.inArray(t["s"], prefres) == -1)
			subj.push(t["s"]);
		if($.inArray(t["o"], obj) == -1 && $.inArray(t["o"], prefres) == -1)
			obj.push(t["o"]);
	}
	
    var nodes = [];
    // create an array with nodes
	for(var i=0; i<prefres.length; i++)
		nodes.push({id: prefres[i], label: prefres[i], color: '#cbacba'});
	for(var i=0; i<subj.length; i++)
		nodes.push({id: subj[i], label: subj[i]});
	for(var i=0; i<obj.length; i++)
		nodes.push({id: obj[i], label: obj[i]});

    // create an array with edges
    var edges = [];
	for(var i=0; i<triples.length; i++) {
		var t = triples[i];
		edges.push({from: t["s"], to: t["o"], label: t["p"], arrows: 'to', font: {size: 12, align: 'horizontal'}});
	}

    // create a network
    var container = document.getElementById("issue-graph");
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {
	    layout: {
	      randomSeed: undefined,
	    },
	    nodes : {
	    	shape: 'dot',
	    	size: 12,
			mass: 4,
	    }
    };
    var network = new vis.Network(container, data, options);
	
}


$( document ).ready(function() {
	params();
	$("#cname").html("<a href='"+cname+"' target='_blank'>"+cname+"</a>");
	$("#alpha").html(alpha);
	$("#graph-panel").hide();
	load_everything();
	
	$('#content').loader({
		
	});
});

