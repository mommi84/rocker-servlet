package org.aksw.rocker.servlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;

public class FileDownloader {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		download("http://rocker.aksw.org/datasets/restaurant1.nt", ""); 
		
	}
	
	public static String download(String url, String datasetPath) {

		String rand = ("" + Math.random()).substring(2);
		
		String fileName = datasetPath + "/" + "local_" + rand + ".nt"; // The file that will be saved
													// on your computer
		URL link;
		try {
			link = new URL(url);

			// Code to download
			InputStream in = new BufferedInputStream(link.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			out.close();
			in.close();
			byte[] response = out.toByteArray();
	
			FileOutputStream fos = new FileOutputStream(fileName);
			fos.write(response);
			fos.close();
			
		// End download code
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IOException: "+e.getMessage());
			e.printStackTrace();
		} 

		System.out.println("File saved to "+fileName);

		return "file://" + fileName;
	}

}
