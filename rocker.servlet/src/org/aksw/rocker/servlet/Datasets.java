package org.aksw.rocker.servlet;

import java.util.HashMap;

public class Datasets {
	
	private static HashMap<String, String> classToNTfile = new HashMap<>();
	
	static {
		classToNTfile.put("OAEI2010 Restaurant1", 
				"/restaurant1.nt");
		classToNTfile.put("OAEI2010 Persons1.1", 
				"/person11.nt");
		classToNTfile.put("DBpedia 3.9", 
				"/monument.nt");
	}
	
	public static String file(String key, String datasetPath) {
		return "file://" + datasetPath + classToNTfile.get(key);
	}

}
