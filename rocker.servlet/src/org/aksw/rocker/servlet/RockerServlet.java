package org.aksw.rocker.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import sun.security.provider.MD5;

import com.hp.hpl.jena.rdf.model.Property;

import de.uni_leipzig.simba.keydiscovery.model.CandidateNode;
import de.uni_leipzig.simba.keydiscovery.model.FaultyPair;
import de.uni_leipzig.simba.keydiscovery.rockerone.Rocker;

public class RockerServlet extends HttpServlet {

	private final static String DATASET = "dataset";
	private final static String CLASS = "cname";
	private final static String ALPHA = "alpha";
	private static String datasetPath = "";
	
	private final static String NAMESPACE = "http://rocker.aksw.org/ontology#";

	private final static Map<String, String> datasets = new HashMap<>();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2209978056533712293L;

	@Override
	public void init() throws ServletException {
		InputStream input;
		try {
			input = RockerServlet.class.getClassLoader().getResourceAsStream(
					"config.properties");
			Properties prop = new Properties();
			// load a properties file
			prop.load(input);

			// get the dataset path
			datasetPath = prop.getProperty("path-datasets");
			loadFileList(datasetPath);
			
			System.out.println("Rocker is launched");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		rocker(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		rocker(req, resp);
	}

	private void loadFileList(String datasetPath) {
		File folder = new File(datasetPath);
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String fileName = listOfFiles[i].getName();
				datasets.put(fileName.substring(0, fileName.lastIndexOf(".")),
						fileName);
			}
		}
		System.out.println("Datasets: " + datasets);
	}

	@SuppressWarnings("unchecked")
	private void rocker(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");

		String className = "http://dbpedia.org/ontology/Monument";
		String dataset = "DBpedia 3.9";
		String datasetName = "rocker" + DigestUtils.md5Hex(System.currentTimeMillis()+"");
		String alpha = "1";

		if (request.getParameter(DATASET) != null)
				dataset = request.getParameter(DATASET);
		
		if (request.getParameter(CLASS) != null)
			className = request.getParameter(CLASS);
		
		String file;
		
		if(dataset.startsWith("http"))
			file = FileDownloader.download(dataset, datasetPath);
		else
			file = Datasets.file(dataset, datasetPath);

		if (request.getParameter(ALPHA) != null)
			alpha = request.getParameter(ALPHA);

		System.out.println("datasetName = "+datasetName);
		System.out.println("dataset = "+dataset);
		System.out.println("className = "+className);
		System.out.println("alpha = "+alpha);
		System.out.println("file = "+file);
		
		Rocker r;
		try {
			r = new Rocker(datasetName, file, className, false, true, 
					new Double(alpha));
			r.run();
			Set<CandidateNode> results = r.getKeys();
			// Set response content type
			response.setContentType("text/json");
			
			JSONObject jo = new JSONObject();
			JSONArray keyArray = new JSONArray();
			
			for(CandidateNode cn : results) {
				JSONObject key = new JSONObject();
				
				JSONArray props = new JSONArray();
				for(Property prop : cn.getProperties())
					props.add(prop.getURI());
				key.put("http://www.w3.org/2004/02/skos/core#member", props);
				
				key.put(NAMESPACE + "score", cn.getScore());
				JSONArray issues = new JSONArray();
				for(FaultyPair pair : cn.getFaultyPairs()) {
					JSONObject issue = new JSONObject();
					JSONArray issueRes = new JSONArray();
					issueRes.add(pair.getSourceURI());
					issueRes.add(pair.getTargetURI());
					issue.put("http://www.w3.org/2004/02/skos/core#member", issueRes);
					issues.add(issue);
				}
				key.put(NAMESPACE + "hasIssues", issues);
				keyArray.add(key);
			}
			
			jo.put("@id", className);
			jo.put("@type", NAMESPACE + "AlmostKey");
			jo.put(NAMESPACE + "hasKey", keyArray);
			
			// Actual logic goes here.
			PrintWriter out = response.getWriter();
			out.println(jo.toJSONString());
			
			if(dataset.startsWith("http"))
				new File(file.substring(7, file.length())).delete();
			
		} catch (Exception e) {
			response.setStatus(501);
			try {
				e.printStackTrace(response.getWriter());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

}
