package org.aksw.rocker.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.atlas.lib.Tuple;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.system.StreamRDF;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.sparql.core.Quad;

import de.uni_leipzig.simba.keydiscovery.model.CandidateNode;
import de.uni_leipzig.simba.keydiscovery.model.FaultyPair;
import de.uni_leipzig.simba.keydiscovery.rockerone.Rocker;

public class ObjSetServlet extends HttpServlet {

	private final static String DATASET = "dataset";
	private final static String CLASS = "class";
	private final static String ALPHA = "alpha";
	private static String datasetPath = "";
//	
//	private final static String NAMESPACE = "http://rocker.aksw.org/ontology#";
//
	private final static Map<String, String> datasets = new HashMap<>();

	/**
	 * 
	 */
	private static final long serialVersionUID = -2209978056533712293L;

	@Override
	public void init() throws ServletException {
		InputStream input;
		try {
			input = ObjSetServlet.class.getClassLoader().getResourceAsStream(
					"config.properties");
			Properties prop = new Properties();
			// load a properties file
			prop.load(input);

			// get the dataset path
			datasetPath = prop.getProperty("path-datasets");
			loadFileList(datasetPath);
			System.out.println("Object Set is launched");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		graph(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		graph(req, resp);
	}

	private void loadFileList(String datasetPath) {
		File folder = new File(datasetPath);
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				String fileName = listOfFiles[i].getName();
				datasets.put(fileName.substring(0, fileName.lastIndexOf(".")),
						fileName);
			}
		}
		System.out.println("Datasets: " + datasets);
	}

	@SuppressWarnings("unchecked")
	private void graph(HttpServletRequest request, HttpServletResponse response) {
		
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
//		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");

		String datasetName = "monument";
		String file = "file://" + datasetPath + "/monument.nt";
		String[] subjects = null, predicates = null;

		if (request.getParameter(DATASET) != null)
			datasetName = request.getParameter(DATASET);

		// comma-separated subject URIs
		if (request.getParameter("subjects") != null)
			subjects = request.getParameter("subjects").split("::");
		final ArrayList<String> subj = new ArrayList<String>(Arrays.asList(subjects));
				
		// comma-separated predicate URIs
		if (request.getParameter("predicates") != null)
			predicates = request.getParameter("predicates").split("::");
		final ArrayList<String> pred = new ArrayList<String>(Arrays.asList(predicates));

		if (datasetName != null) {
			if(datasetName.startsWith("http")) {
				file = FileDownloader.download(datasetName, datasetPath);
			} else {
				file = Datasets.file(datasetName, datasetPath);
			}
		}
		
		final Bean bean = new Bean();
		
		StreamRDF dest = new StreamRDF() {

			@Override
			public void base(String arg0) {}

			@Override
			public void finish() {}

			@Override
			public void prefix(String arg0, String arg1) {}

			@Override
			public void quad(Quad arg0) {}

			@Override
			public void start() {}

			@Override
			public void triple(Triple arg0) {
				if(subj.contains(arg0.getSubject().getURI())
						&& (pred.contains(arg0.getPredicate().getURI())
								|| arg0.getPredicate().getURI().equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))
				)
					bean.triples.add(arg0);
			}

			@Override
			public void tuple(Tuple<Node> arg0) {}
			
		};
		
		RDFDataMgr.parse(dest, file);
		
		System.out.println("datasetName = "+datasetName);
		System.out.println("subj = "+subj);
		System.out.println("pred = "+pred);
		System.out.println("file = "+file);
		System.out.println("bean.triples = "+bean.triples);
		
		try {
			// Set response content type
			response.setContentType("text/json");
			
			JSONObject jo = new JSONObject();
			JSONArray triples = new JSONArray();
			for(Triple t : bean.triples) {
				JSONObject trp = new JSONObject();
				trp.put("s", t.getSubject().getURI());
				trp.put("p", t.getPredicate().getURI());
				trp.put("o", t.getObject().toString().replaceAll("\"", ""));
				triples.add(trp);
			}
			jo.put("triples", triples);
			
			// Actual logic goes here.
			PrintWriter out = response.getWriter();
			out.println(jo.toJSONString());
			
			if(datasetName.startsWith("http"))
				new File(file.substring(7, file.length())).delete();
			
			
		} catch (Exception e) {
			response.setStatus(501);
			try {
				e.printStackTrace(response.getWriter());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

}

class Bean {
	protected ArrayList<Triple> triples = new ArrayList<>();
}