package org.aksw.rocker.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OntologyServlet extends HttpServlet {
	
	private static String ONTOLOGY_PATH;

	/**
	 * 
	 */
	private static final long serialVersionUID = -2209978056533712293L;

	@Override
	public void init() throws ServletException {
		System.out.println("RDF ontology requested.");
		
		InputStream input;
		try {
			input = RockerServlet.class.getClassLoader().getResourceAsStream(
					"config.properties");
			Properties prop = new Properties();
			// load a properties file
			prop.load(input);

			// get the dataset path
			ONTOLOGY_PATH = prop.getProperty("path-datasets") + "/../rdf/rocker-ontology.rdf";
			
			System.out.println("Rocker is launched");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ontology(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ontology(req, resp);
	}


	private void ontology(HttpServletRequest request, HttpServletResponse response) {
		
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET");
//		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");

		try {
			// Set response content type
			response.setContentType("application/rdf+xml");
			
			Scanner in = new Scanner(new File(ONTOLOGY_PATH));
			
			// Actual logic goes here.
			PrintWriter out = response.getWriter();
			while(in.hasNextLine())
				out.println(in.nextLine());
			in.close();
			
		} catch (Exception e) {
			response.setStatus(501);
			try {
				e.printStackTrace(response.getWriter());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

}
