This is a code repository for ROCKER servlet, which will run the project as a RESTful service, as well as a host for [datasets](https://bitbucket.org/mommi84/rocker-servlet/downloads).

For public releases, please visit [GitHub](https://github.com/AKSW/rocker).